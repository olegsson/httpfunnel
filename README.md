# httpfunnel

Tiny http service that funnels unix domain sockets from a directory into a single api.

## Install

`go get -u gitlab.com/olegsson/httpfunnel`

## Usage
```
httpfunnel -p PORT -d DIR

  -d string
    	dir with sockets to funnel (default "./")
  -p string
    	port to serve on (default "1337")
```

## Example
To serve `/path/to/dir/socket1` and `/path/to/dir/socket2` over `http://0.0.0.0:5000/socket1` and `http://0.0.0.0:5000/socket2`, respectively:
```
httpfunnel -p 5000 -d /path/to/dir
```
