package main

import (
    "fmt"
    "flag"
    "path"
    "strings"
    "net/url"
    "net/http"
    "net/http/httputil"
    "github.com/tv42/httpunix"
)

func makeFunnel(directory *string) func(res http.ResponseWriter, req *http.Request) {
    sockdir := path.Clean(*directory) + "/"
    return func(res http.ResponseWriter, req *http.Request) {
        levels := strings.Split(req.URL.Path, "/")
        name := levels[1]

        req.URL.Path = "/" + strings.Join(levels[2:], "/")

        transport := &httpunix.Transport{}
        transport.RegisterLocation(name, sockdir+name)

        proxyUrl, _ := url.Parse("http+unix://"+name)
        proxy := httputil.NewSingleHostReverseProxy(proxyUrl)
        proxy.Transport = transport
        proxy.ServeHTTP(res, req)
    }
}

func main() {
    port := flag.String("p", "1337", "port to serve on")
    directory := flag.String("d", "./", "dir with sockets to funnel")
    flag.Parse()
    fmt.Println("funneling unix sockets from:", *directory)
    fmt.Println("listening on :"+*port+"\n")

    http.HandleFunc("/", makeFunnel(directory))
    if err := http.ListenAndServe(":"+*port, nil); err != nil {
        panic(err)
    }
}
